const buttonAdded = document.querySelector(".button2");
const popUpWindow = document.querySelector(".modal");
const closeButton = document.querySelector(".btn-close");
const cardiologistForm = document.querySelector(".cardiologist-form");
const teethCareForm = document.querySelector(".teeth-care-form");
const therapistForm = document.querySelector(".therapist");

buttonAdded.addEventListener("click", function (e) {
  popUpWindow.classList.add("show");
  popUpWindow.style.display = "block";
});

closeButton.addEventListener("click", function (e) {
  popUpWindow.classList.remove("show");
  popUpWindow.style.display = "none";
});

doctorSelect.addEventListener("change", function () {
  if (this.value === "Кардиолог") {
    cardiologistForm.style.display = "block";
    teethCareForm.style.display = "none";
    therapistForm.style.display = "none";
  } else if (this.value === "Стоматолог") {
    teethCareForm.style.display = "block";
    cardiologistForm.style.display = "none";
    therapistForm.style.display = "none";
  } else if (this.value === "Терапевт") {
    therapistForm.style.display = "block";
    teethCareForm.style.display = "block";
    cardiologistForm.style.display = "none";
  } else {
    cardiologistForm.style.display = "none";
    teethCareForm.style.display = "none";
    therapistForm.style.display = "none";
  }
});
