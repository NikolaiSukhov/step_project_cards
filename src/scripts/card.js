class Card {
  constructor(
    purpose,
    description,
    urgency,
    fullName,
    doctor,
    normalPressure,
    weight,
    heartDiseases,
    age,
    lastVisit,
    id
  ) {
    this.purpose = purpose;
    this.description = description;
    this.urgency = urgency;
    this.fullName = fullName;
    this.doctor = doctor;
    this.normalPressure = normalPressure;
    this.weight = weight;
    this.heartDiseases = heartDiseases;
    this.age = age;
    this.lastVisit = lastVisit;
    this.id = id;
  }

  render() {
    const container = document.querySelector(".cards");

    const card = document.createElement("div");
    card.classList.add("card", "card-body");
    card.setAttribute("id", this.id);
    // Handlers for buttons
    const showMore = () => {
      const additionalInfo = document.querySelector(`.additional-${this.id}`);
      additionalInfo.classList.toggle("active");
      if (btnMore.innerText === "More") {
        btnMore.innerText = "Less";
      } else {
        btnMore.innerText = "More";
      }
    };

    const cardDelete = () => {
      const url = `https://ajax.test-danit.com/api/v2/cards/${this.id}`;
      fetch(url, {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer 2d36f854-b3c7-4615-8b91-17fd76170d10`,
        },
      });
      document.getElementById(`${this.id}`).remove();
    };

    const cardEdit = (id) => {
      document.querySelector(".modal").classList.add("show");
      document.querySelector(".modal").style.display = "block";

      document
        .querySelector(".asigments-form-1")
        .addEventListener("submit", function () {
          let selectedDoctor = doctorSelect.value;
          let enteredDestination = destinationInput.value;
          let enteredDescription = descriptionInput.value;
          let selectedUrgency = urgencySelect.value;
          let enteredName = nameInput.value;
          let enteredAge = ageInput.value;
          let enteredMass = mass.value;
          let enteredBloodPressure = bloodpressure.value;
          let enteredDate = date.value;
          let enteredIllness = illness.value;

          const data = {
            selectedDoctor,
            enteredDestination,
            enteredDescription,
            selectedUrgency,
            enteredName,
            enteredAge,
            enteredMass,
            enteredBloodPressure,
            enteredDate,
            enteredIllness,
          };
          const url = `https://ajax.test-danit.com/api/v2/cards/${id}`;
          fetch(url, {
            method: "PUT",
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer 2d36f854-b3c7-4615-8b91-17fd76170d10`,
            },
            body: JSON.stringify(data),
          });
        });
    };
    //КНОПКА ПОКАЗАТИ БІЛЬШЕ

    const btnMore = document.createElement("button");
    btnMore.setAttribute("id", "btn-more");
    btnMore.innerText = "More";
    btnMore.addEventListener("click", showMore);
    btnMore.classList.add("btn", "btn-all");
    card.append(btnMore);

    //КНОПКА РЕДАГУВАТИ

    const btnEdit = document.createElement("button");
    btnEdit.setAttribute("id", "btn-edit");
    btnEdit.innerText = "Edit";
    btnEdit.addEventListener("click", () => cardEdit(this.id));
    btnEdit.classList.add("btn", "btn-edit", "btn-all");

    card.append(btnEdit);

    //КНОПКА ВИДАЛИТИ
    const btnDelete = document.createElement("button");
    btnDelete.setAttribute("id", "btn-delete");
    btnDelete.addEventListener("click", cardDelete);

    card.append(btnDelete);

    card.insertAdjacentHTML(
      "afterbegin",
      ` <div class="card-info">
          <h3 class="card-title">${this.fullName}</h3>
          <p class="card-text">${this.doctor}</p>
          <div class="additional-info additional-${this.id}">
            <p>${this.purpose}</p>
            <p>${this.description}</p>
            <p>${this.urgency}</p>
            <p>${this.normalPressure ? this.normalPressure : ""} </p>
            <p>${this.weight ? this.weight : ""}</p>
            <p>${this.heartDiseases ? this.heartDiseases : ""}</p>
            <p>${this.age ? this.age : ""}</p>
            <p>${this.lastVisit ? this.lastVisit : ""}</p>
          </div>
        </div>
      `
    );
    container.append(card);
  }
}



fetch("https://ajax.test-danit.com/api/v2/cards", {
  method: "GET",
  headers: {
    "Content-Type": "application/json",
    Authorization: `Bearer 2d36f854-b3c7-4615-8b91-17fd76170d10`,
  },
})
  .then((res) => res.json())
  .then((res) =>
    res.forEach((item) => {
      console.log(item);
      const card = new Card(
        item.destanation,
        item.description,
        item.urgency,
        item.nameinput,
        item.doctor,
        item.bloodpreassurevalue,
        item.massvalue,
        item.illnesvalue,
        item.age,
        item.datevalue,
        item.id
      );
      console.log(card);
      card.render();
    })
  );
