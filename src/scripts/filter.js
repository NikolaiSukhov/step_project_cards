const statusis = document.getElementById('statusid');
const formsearch = document.querySelector('.form-search');
const urgencyid = document.getElementById('urgencyid').value;
const searchbtn = document.querySelector('.submit-serachform');
const getID = document.querySelector('.getid').value;

function getAllCards() {
  axios
    .get('https://ajax.test-danit.com/api/v2/cards', {
      headers: {
        Authorization: 'Bearer 2d36f854-b3c7-4615-8b91-17fd76170d10',
      },
    })
    .then(response => {
      console.log(response.data);
    })
    .catch(error => {
      console.error(error);
    });
}

const cardContainer = document.getElementById('cardContainer');

function getCardByID() {
  const getID = document.querySelector('.getid').value;

  axios
    .get(`https://ajax.test-danit.com/api/v2/cards/${getID}`, {
      headers: {
        Authorization: 'Bearer 2d36f854-b3c7-4615-8b91-17fd76170d10',
      },
    })
    .then(response => {
      console.log(response.data);

      const cardData = response.data;

      cardContainer.innerHTML = '';

      for (const key in cardData) {
        const p = document.createElement('p');
        p.textContent = `${key}: ${cardData[key]}`;
        cardContainer.appendChild(p);
      }
    })
    .catch(error => {
      console.error(error);
    });
}

formsearch.addEventListener('submit', function(e) {
  e.preventDefault();
  getCardByID();
});

function filterByTitleAndContent(keyword) {
  axios
    .get('https://ajax.test-danit.com/api/v2/cards', {
      headers: {
        Authorization: 'Bearer 2d36f854-b3c7-4615-8b91-17fd76170d10',
      },
    })
    .then(response => {
      const filteredCards = response.data.filter(card => {
        return (
          card.destanation.toLowerCase().includes(keyword.toLowerCase()) ||
          card.description.toLowerCase().includes(keyword.toLowerCase())
        );
      });

      console.log(filteredCards);
    })
    .catch(error => {
      console.error(error);
    });
}

formsearch.addEventListener('submit', function(e) {
  e.preventDefault();
  const keyword = document.querySelector('.getid').value;
  filterByTitleAndContent(keyword);
});

function filterByVisitStatusAndUrgency(visitStatus, urgency) {
  axios
    .get('https://ajax.test-danit.com/api/v2/cards', {
      headers: {
        Authorization: 'Bearer 2d36f854-b3c7-4615-8b91-17fd76170d10',
      },
    })
    .then(response => {
      const filteredCards = response.data.filter(card => {
        return (
          card.urgency === urgency &&
          ((visitStatus === 'open' && !card.done) ||
            (visitStatus === 'done' && card.done))
        );
      });

      console.log(filteredCards);
    })
    .catch(error => {
      console.error(error);
    });
}

formsearch.addEventListener('submit', function(e) {
  e.preventDefault();
  const visitStatus = document.getElementById('statusid').value.toLowerCase();
  const urgency = document.getElementById('urgencyid').value;
  filterByVisitStatusAndUrgency(visitStatus, urgency);
});

getAllCards();
