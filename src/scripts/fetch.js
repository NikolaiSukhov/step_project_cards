const doctorSelect = document.querySelector("#doctor");
const destinationInput = document.querySelector(
  '.form-control[placeholder="Цель визита"]'
);
const descriptionInput = document.querySelector(
  '.form-control[placeholder="Краткое описание визита"]'
);
const urgencySelect = document.querySelector(".urgency");
const nameInput = document.querySelector('.form-control[placeholder="ФИО"]');
const form = document.querySelector(".asigments-form-1");
const ageInput = document.querySelector('.form-control[placeholder="Возраст"]');
const mass = document.querySelector(
  '.form-control[placeholder="Индекс массы тела"]'
);
const bloodpressure = document.querySelector(
  '.form-control[placeholder="Обычное давление"]'
);
const date = document.querySelector(
  '.form-control[placeholder="Дата последнего посещения"]'
);
const illness = document.querySelector(
  '.form-control[placeholder="Перенесенные заболевания сердечно-сосудистой системы"]'
);

class Visit {
  constructor(
    doctor,
    destanation,
    description,
    urgency,
    nameinput,
    age,
    massvalue,
    bloodpressurevalue,
    datevalue,
    illnessvalue
  ) {
    this.doctor = doctor;
    this.destanation = destanation;
    this.description = description;
    this.urgency = urgency;
    this.nameinput = nameinput;
    this.age = age;
    this.massvalue = massvalue;
    this.bloodpressurevalue = bloodpressurevalue;
    this.datevalue = datevalue;
    this.illnessvalue = illnessvalue;
  }
}
function submitEvent(e) {
  e.preventDefault();

  let selectedDoctor = doctorSelect.value;
  let enteredDestination = destinationInput.value;
  let enteredDescription = descriptionInput.value;
  let selectedUrgency = urgencySelect.value;
  let enteredName = nameInput.value;
  let enteredAge = ageInput.value;
  let enteredMass = mass.value;
  let enteredBloodPressure = bloodpressure.value;
  let enteredDate = date.value;
  let enteredIllness = illness.value;

  let visit = new Visit(
      selectedDoctor,
      enteredDestination,
      enteredDescription,
      selectedUrgency,
      enteredName,
      enteredAge,
      enteredMass,
      enteredBloodPressure,
      enteredDate,
      enteredIllness
  );

  fetch("https://ajax.test-danit.com/api/v2/cards", {
      method: 'POST',
      headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer 2d36f854-b3c7-4615-8b91-17fd76170d10'
      },
      body: JSON.stringify(visit)
  })
  .then(response => response.json())
  .then(response => console.log(response));
}

form.addEventListener('submit', submitEvent);
